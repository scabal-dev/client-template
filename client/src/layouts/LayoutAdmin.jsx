import React from "react";
import { Route, Switch } from "react-router-dom";
import { Layout } from "antd";
import "./LayoutAdmin.scss";

const LayoutAdmin = (props) => {
  const { routes } = props;
  const { Header, Content, Footer } = Layout;
  console.log(props);

  return (
    <Layout>
      <h2>Menu sidebar Admin</h2>
      <Layout>
        <Header>Header...</Header>
        <Content>
          <LoadRouters routes={routes} />
        </Content>
        <Footer>Samuel Cabal Lozano</Footer>
      </Layout>
    </Layout>
  );
};

const LoadRouters = ({ routes }) => {
  return (
    <Switch>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />
      ))}
    </Switch>
  );
};

export default LayoutAdmin;
