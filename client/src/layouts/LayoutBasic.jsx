import React from "react";
import { Route, Switch } from "react-router-dom";
import { Layout } from "antd";
import "./LayoutBasic.scss";

const LayoutBasic = (props) => {
  const { routes } = props;
  const { Header, Footer, Content } = Layout;
  console.log(props);

  return (
    <Layout>
      <h2>Menu Sider basic user...</h2>
      <Layout>
        <Header>Header...</Header>
        <Content>
          <LoadRoutes routes={routes} />
        </Content>
        <Footer>
          <h5>Samuel Cabal Lozano</h5>
        </Footer>
      </Layout>
    </Layout>
  );
};

const LoadRoutes = ({ routes }) => {
  return (
    <Switch>
      {routes.map((value, index) => (
        <Route
          key={index}
          path={value.path}
          exact={value.exact}
          component={value.component}
        />
      ))}
    </Switch>
  );
};

export default LayoutBasic;
